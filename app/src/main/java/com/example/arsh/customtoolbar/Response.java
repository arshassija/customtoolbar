package com.example.arsh.customtoolbar;

import java.io.Serializable;

public class Response implements Serializable {

    private AllAirPort all_airport;
    private String token;
    private String login;

    public AllAirPort getAll_airport() {
        return all_airport;
    }

    public void setAll_airport(AllAirPort all_airport) {
        this.all_airport = all_airport;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
