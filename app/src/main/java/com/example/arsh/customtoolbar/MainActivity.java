package com.example.arsh.customtoolbar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String responseStringFromVolley = "{\n" +
                "    \"all_airport\":{\n" +
                "        \"airport\":[\n" +
                "                {\n" +
                "                \"airport_name\":\"Mali\",\n" +
                "                \"airport_code\":\"ARD\",\n" +
                "                \"location_name\":\"Alor Island\",\n" +
                "                \"country_id\":\"id\",\n" +
                "                \"country_name\":\"Indonesia\"\n" +
                "                },\n" +
                "                {\n" +
                "                \"airport_name\":\"Pattimura\",\n" +
                "                \"airport_code\":\"AMQ\",\n" +
                "                \"location_name\":\"Ambon\",\n" +
                "                \"country_id\":\"id\",\n" +
                "                \"country_name\":\"Indonesia\"\n" +
                "                },\n" +
                "                {\n" +
                "                \"airport_name\":\"Tanjung Api\",\n" +
                "                \"airport_code\":\"VPM\",\n" +
                "                \"location_name\":\"Ampana\",\n" +
                "                \"country_id\":\"id\",\n" +
                "                \"country_name\":\"Indonesia\"\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "    \"token\":\"ab4f5e12e794ab09d49526bc75cf0a0139d9d849\",\n" +
                "    \"login\":\"false\"\n" +
                "}";
        Response response = new Gson().fromJson(responseStringFromVolley, Response.class);
        Toast.makeText(this, response.getToken(), Toast.LENGTH_SHORT).show();
    }
}
