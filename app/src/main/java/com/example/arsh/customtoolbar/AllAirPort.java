package com.example.arsh.customtoolbar;

import java.io.Serializable;
import java.util.ArrayList;

public class AllAirPort implements Serializable{

    private ArrayList<AirportModel> airport;

    public ArrayList<AirportModel> getAirport() {
        return airport;
    }

    public void setAirport(ArrayList<AirportModel> airport) {
        this.airport = airport;
    }
}
